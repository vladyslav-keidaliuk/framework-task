﻿using Core.Configuration;
using Core.UtilityLibrary;
using Core;
using NUnit.Framework.Interfaces;
using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FrameworkTests
{
    [TestFixture]
    [Category("MailTest")]
    public class MailTests
    {
        private IWebDriver driver;
        private Listener listener;

        [SetUp]
        public void Setup()
        {
            var browser = (Drivers)Enum.Parse(typeof(Drivers), Configuration.Model.Browser);

            Console.WriteLine("=============");
            Console.WriteLine(Configuration.Model.Browser);
            Console.WriteLine("=============");
            Console.WriteLine("MailTest");

            driver = DriverProvider.GetDriverFactory(browser).CreateDriver();
            driver.Manage().Window.Position = new Point(0, -1000);
            driver.Manage().Window.Maximize();

            listener = new Listener(driver);
            driver.Url = Configuration.Model.MailServiceUrl;
        }

        [Test]
        public void LogInTest()
        {
            var startPageMail = new Pages.MailService.StartPage(driver);
            string generatedMail = TextGenerator.GenerateRandomText(30);
            startPageMail.CreateOrLogInTempMail(generatedMail);
            generatedMail = generatedMail + "@yopmail.com";
            string mail = startPageMail.GetMail();
            Console.WriteLine(mail);
            Assert.That(mail, Is.EqualTo(generatedMail));
        }


        [TearDown]
        public void TearDown()
        {
            if (TestContext.CurrentContext.Result.Outcome.Status == TestStatus.Failed)
            {
                listener.CaptureScreenshot(TestContext.CurrentContext.Test.FullName);
            }
            driver.Close();
            driver.Quit();
        }
    }
}
