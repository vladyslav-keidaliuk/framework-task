using Core.Configuration;
using Core;
using System.Drawing;
using NUnit.Framework.Internal;
using OpenQA.Selenium;
using Pages;
using System.Globalization;
using Core.UtilityLibrary;
using NUnit.Framework.Interfaces;

namespace FrameworkTests
{
    [TestFixture]
    [Category("Smoke")]
    public class Tests
    {
        private IWebDriver driver;
        private Listener listener;

        [SetUp]
        public void Setup()
        {
            var browser = (Drivers)Enum.Parse(typeof(Drivers), Configuration.Model.Browser);

            Console.WriteLine("=============");
            Console.WriteLine(Configuration.Model.Browser);
            Console.WriteLine("=============");
            Console.WriteLine("Smoke Test");
            
            driver = DriverProvider.GetDriverFactory(browser).CreateDriver();
            driver.Manage().Window.Position = new Point(0, -1000);
            driver.Manage().Window.Maximize();

            listener = new Listener(driver);

            driver.Url = Configuration.Model.GCloudUrl;
        }

        [Test]
        public void SmokeTest()
        {

            var startPage = new Pages.GoogleCloud.StartPage(driver);
            startPage.SearchCalculator();
            startPage.OpenCalculator();

            var calculatorPage = new Pages.GoogleCloud.CalculatorPage(driver);
            calculatorPage.ClickOnComputeEngine();

            calculatorPage.InputNumberOfInstances();
            calculatorPage.SelectSeries();
            calculatorPage.SelectMachineType();
            calculatorPage.CheckBoxAddGPUs();
            calculatorPage.SelectGPUType();
            calculatorPage.Sele�tNumberOfGPUs();
            calculatorPage.SelectLocalSSD();
            calculatorPage.SelectDatacenterLocation();
            calculatorPage.SelectCommitedUsage();
            calculatorPage.ClickAddToEstimate();

            string costFromCalculator = calculatorPage.GetTotalEstimatedMonthlyCost();


            calculatorPage.OpenEmailEstimateForm();

            var tabController = new DriverController(driver);

            tabController.OpenEmailPage();

            var startPageMail = new Pages.MailService.StartPage(driver);

            string generatedMail = TextGenerator.GenerateRandomText(30);

            startPageMail.CreateOrLogInTempMail(generatedMail);

            string mail = startPageMail.GetMail();
            Console.WriteLine(mail);


            tabController.SwitchToGoogleCloudPage();

            calculatorPage.SendEstimateForm(mail);

            tabController.SwitchToEmailPage();

            string costFromEmail = startPageMail.GetCostFromEmail();

            Assert.That(costFromEmail, Is.EqualTo(costFromCalculator));

        }

        
        [TearDown]
        public void TearDown()
        {
            if (TestContext.CurrentContext.Result.Outcome.Status == TestStatus.Failed)
            {
                listener.CaptureScreenshot(TestContext.CurrentContext.Test.FullName);
            }
            driver.Close();
            driver.Quit();
        }
    }
}