﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FrameworkTests
{
    public class Listener
    {
        readonly IWebDriver driver;
        public Listener(IWebDriver driver)
        {
            this.driver = driver;
        }
        public void CaptureScreenshot(string testName)
        {
            ITakesScreenshot screenshot = (ITakesScreenshot)driver;
            Screenshot shot = screenshot.GetScreenshot();
            string timestamp = DateTime.Now.ToString("yyyy_MM_dd_HH_mm_ss_fff");
            string screenshotFileName = $"{testName}_{timestamp}_FailureScreenshot.png";

            string folderName = @"..\..\..\errors\";

            string artifactsDirectory = Path.Combine(folderName);
            string screenshotPath = Path.Combine(artifactsDirectory, screenshotFileName);

            Console.WriteLine(screenshotPath);

            if (!Directory.Exists(artifactsDirectory))
            {
                Directory.CreateDirectory(artifactsDirectory);
            }

            shot.SaveAsFile(screenshotPath);
        }
    }
}
