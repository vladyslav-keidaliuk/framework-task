# Framework

## Hardcore Webdriver Task

When completing the task, you must use the capabilities of Selenium WebDriver, the unit test framework and the Page Object pattern. Automate the following scenario:
 
1. Open https://cloud.google.com/ 
2. Click the portal search button at the top of the page, enter “Google Cloud Platform Pricing Calculator” in the search field
3. Start the search by clicking the search button.
4. In the search results, click “Google Cloud Platform Pricing Calculator” and go to the calculator page.
5. Activate the COMPUTE ENGINE section at the top of the page
6. Fill out the form with the following data:
     * Number of instances: 4
     * What are these instances for?: leave blank
     * Operating System / Software: Free: Debian, CentOS, CoreOS, Ubuntu, or other User Provided OS
     *VM Class: Regular
     * Instance type: n1-standard-8 (vCPUs: 8, RAM: 30 GB)
     * Select Add GPUs
         * Number of GPUs: 1
         * GPU type: NVIDIA Tesla V100
     * Local SSD: 2x375 Gb
     * Datacenter location: Frankfurt (europe-west3)
     * Committed usage: 1 Year
7. Click "Add to Estimate"
8. Select EMAIL ESTIMATE
9. In a new tab, open https://yopmail.com/ or a similar service to generate temporary emails
10. Copy the email address generated in yopmail.com
11. Return to the calculator, enter the copied address in the Email field
12. Click SEND EMAIL
13. Wait for the letter with the cost calculation and check that the "Total Estimated Monthly Cost" in the letter matches what is displayed in the calculator

## What should be done

Develop an automation framework for the Hardcore task in WebDriver.
The final framework should include the following:
- A WebDriver manager for managing browser connectors
- Page Object/Page Factory for page abstractions
- Models for business objects of the required elements
- Property files with test data for at least two different environments
- XML suites for smoke tests and other tests
- If the test fails, a screenshot with the date and time is taken.
- The framework should include an option for running with Jenkins and browser parameterization, test suite, environment.
- Test results should be displayed on the job chart, and the screenshots should be archived as artifacts.

