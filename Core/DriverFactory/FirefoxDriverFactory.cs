﻿using OpenQA.Selenium.Edge;
using OpenQA.Selenium;
using OpenQA.Selenium.Firefox;

namespace Core.DriverFactory;

public class FirefoxDriverFactory : BaseDriverFactory
{
    public override IWebDriver CreateDriver()
    {
        var options = new FirefoxOptions();
        return new FirefoxDriver(options);
    }
}