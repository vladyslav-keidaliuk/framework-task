﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Configuration
{
    public class ConfigModel
    {
        public string Browser { get; set; }
        public string Log { get; set; }
        public string GCloudUrl { get; set; }

        public string MailServiceUrl { get; set; }
    }
}
