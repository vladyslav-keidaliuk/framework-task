﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;

namespace Core.Configuration
{
    public static class Configuration
    {
        public static ConfigModel Model { get; }

        static Configuration()
        {
            string file = Environment.GetEnvironmentVariable("CONFIG_FILE") ?? "configChrome.json";
            Model = new ConfigModel();
            new ConfigurationBuilder()
                .AddJsonFile(@$"./Configuration/{file}")
                .Build()
                .Bind(Model);
        }
    }
}
