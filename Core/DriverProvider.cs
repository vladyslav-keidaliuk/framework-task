﻿using Core.DriverFactory;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium;
using OpenQA.Selenium.Edge;
using OpenQA.Selenium.Firefox;

namespace Core
{
    public enum Drivers
    {
        Chrome,
        Edge,
        Firefox
    }
    public class DriverProvider
    {
        public static IWebDriver GetDriver(Drivers driver)
        {
            return driver switch
            {
                Drivers.Chrome => new ChromeDriver(),
                Drivers.Edge => new EdgeDriver(),
                Drivers.Firefox => new FirefoxDriver(),
                _ => throw new NotSupportedException()
            };

        }

        public static BaseDriverFactory GetDriverFactory(Drivers driver)
        {
            return driver switch
            {
                Drivers.Chrome => new ChromeDriverFactory(),
                Drivers.Edge => new EdgeDriverFactory(),
                Drivers.Firefox => new FirefoxDriverFactory(),
                _ => throw new NotSupportedException()
            };
        }
    }
}