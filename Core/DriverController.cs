﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core
{
    public class DriverController
    {
        private readonly IWebDriver driver;

        public DriverController(IWebDriver driver)
        {
            this.driver = driver;
        }
        public void OpenEmailPage()
        {
            ((IJavaScriptExecutor)driver).ExecuteScript("window.open();");

            Thread.Sleep(2000);

            driver.SwitchTo().Window(driver.WindowHandles.Last());
            driver.Navigate().GoToUrl(Configuration.Configuration.Model.MailServiceUrl);
        }

        public void SwitchToGoogleCloudPage()
        {
            driver.SwitchTo().Window(driver.WindowHandles[0]);
        }

        public void SwitchToEmailPage()
        {
            driver.SwitchTo().Window(driver.WindowHandles[1]);
        }
    }
}
