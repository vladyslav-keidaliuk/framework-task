﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;

namespace Pages.MailService;

public class StartPage : BasePage
{
    private readonly By InputFieldForCreateMail =
        By.XPath("//body//form/div/div/div[2]/div/input");

    private readonly By EmailLabel = 
        By.XPath("/html/body/div[1]/div/main/div[1]/div/div[1]");

    private readonly By RefreshButton =
        By.CssSelector("#refresh");

    private readonly By LastLetter =
        By.XPath("/html/body/div[2]/div[2]/button");

    private readonly By FrameWithLetters =
        By.XPath("/html/body/div[1]/div/main/div[2]/div[1]/div/div[3]/iframe[1]");

    private readonly By FrameWithCurrentLetter =
        By.CssSelector("#ifmail");

    private readonly By EstimatedCost =
        By.CssSelector(
                "#mail > div > div > table > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(2) > td:nth-child(2) > h3");

    public StartPage(IWebDriver driver) : base(driver)
    {
        this.driver = driver;
    }

    public void CreateOrLogInTempMail(string mail)
    {
        var waiter = new WebDriverWait(driver, TimeSpan.FromSeconds(5));
        var element = waiter.Until(w => w.FindElement(InputFieldForCreateMail));
        element.SendKeys(mail);
        element.SendKeys(Keys.Enter);

    }

    public string GetMail()
    {
        var waiter = new WebDriverWait(driver, TimeSpan.FromSeconds(5));
        var element = waiter.Until(w => w.FindElement(EmailLabel));
        string email = element.Text;
        return email;
    }

    public string GetCostFromEmail()
    {
        Thread.Sleep(10000);
        var waiter = new WebDriverWait(driver, TimeSpan.FromSeconds(5));
        var element = waiter.Until(w => w.FindElement(RefreshButton));
        element.Click();

        element = driver.FindElement(FrameWithLetters);
        driver.SwitchTo().Frame(element);

        element = waiter.Until(w => w.FindElement(LastLetter));
        element.Click();

        driver.SwitchTo().DefaultContent();

        element = driver.FindElement(FrameWithCurrentLetter);
        driver.SwitchTo().Frame(element);

        element = waiter.Until(w => w.FindElement(EstimatedCost));

        string cost = element.Text;

        return cost;
    }

}