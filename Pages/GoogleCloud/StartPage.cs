﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;

namespace Pages.GoogleCloud
{
    public class StartPage : BasePage
    {

        private readonly By SearchButton = By.XPath("//*[@id=\"kO001e\"]/div[2]/div[1]/div/div[2]/div[2]/div[1]/form/div/input");

        private readonly By CalculatorUrl =
            By.XPath("//*[@id=\"___gcse_0\"]//div[5]/div[2]//div[1]/div[1]/div[1]/div[1]/div/a/b");

        public StartPage(IWebDriver driver) : base(driver)
        {
            this.driver = driver;
        }

        public void SearchCalculator()
        {
            var waiter = new WebDriverWait(driver, TimeSpan.FromSeconds(5));
            Thread.Sleep(3000);
            var Entry = waiter.Until(w => w.FindElement(SearchButton));
            Thread.Sleep(3000);
            Entry.Click();
            Entry.SendKeys("Google Cloud Platform Pricing Calculator");
            Entry.SendKeys(Keys.Enter);

        }

        public void OpenCalculator()
        {
            var waiter = new WebDriverWait(driver, TimeSpan.FromSeconds(5));
            var element = waiter.Until(w => w.FindElement(CalculatorUrl));
            element.Click();
        }
    }
}