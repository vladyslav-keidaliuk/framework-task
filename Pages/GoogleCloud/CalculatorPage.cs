﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;

namespace Pages.GoogleCloud;

public class CalculatorPage
{
    private IWebDriver driver;
    private readonly By IFrame1BeforeComputeEngine = By.XPath("//*[@id=\"cloud-site\"]/devsite-iframe/iframe");
    private readonly By IFrame2BeforeComputeEngine = By.XPath("//*[@id=\"myFrame\"]");
    private readonly By ComputeEngine = By.XPath("//*[@id=\"tab-item-1\"]/div[1]/div");

    private readonly 
        By NumberOfInstances =
        By.XPath(
            "/html/body/md-content/md-card/div/md-card-content[1]/div[2]/div/md-card/md-card-content/div/div[1]/form/div[1]/div[1]/md-input-container/input");

    private readonly 
        By ChooseSeries =
        By.XPath(
            "/html/body/md-content/md-card/div/md-card-content[1]/div[2]/div/md-card/md-card-content/div/div[1]/form/div[6]/div[1]/md-input-container/md-select/md-select-value/span[2]");

    private readonly 
        By SeriesN1 =
        By.XPath("/html/body/div[4]/md-select-menu/md-content/md-option[1]");

    private readonly 
        By ChooseMachineType =
        By.XPath("/html/body/md-content/md-card/div/md-card-content[1]/div[2]/div/md-card/md-card-content/div/div[1]/form/div[7]/div[1]/md-input-container/md-select/md-select-value/span[2]");

    private readonly 
        By MachineType =
        By.XPath("/html/body/div[5]/md-select-menu/md-content/md-optgroup[3]/md-option[4]");

    private readonly 
        By AddGPUs =
        By.XPath(
            "/html/body/md-content/md-card/div/md-card-content[1]/div[2]/div/md-card/md-card-content/div/div[1]/form/div[13]/div[1]/md-input-container/md-checkbox/div[1]");

    private readonly 
        By ChooseGPUType =
        By.XPath("/html/body/md-content/md-card/div/md-card-content[1]/div[2]/div/md-card/md-card-content/div/div[1]/form/div[14]/div/div[1]/div[1]/md-input-container[1]/md-select");

    private readonly 
        By GPUTypeTeslaV100 =
        By.XPath("/html/body/div[6]/md-select-menu/md-content/md-option[4]");

    private readonly 
        By ChooseNumberOfGPUs =
        By.XPath("//body//md-card-content[1]//md-card/md-card-content//form/div[14]/div/div[1]/div[1]//md-input-container[2]/md-select/md-select-value/span[2]");

    private readonly 
        By NumberOfGPUs1 =
        By.XPath("/html/body/div[7]/md-select-menu/md-content/md-option[2]");

    private readonly 
        By ChooseLocalSSD =
        By.XPath("//body/md-content//md-card-content[1]/div[2]//div/div[1]/form/div[15]/div[1]//md-select-value/span[2]");

    private readonly 
        By LocalSSD2x375GB =
        By.XPath("/html/body/div[8]/md-select-menu/md-content/md-option[3]");

    private readonly 
        By ChooseDatacenterLocation =
        By.XPath("//body//md-card-content[1]/div[2]//div[16]//md-select/md-select-value/span[2]");

    private readonly 
        By DatacenterLocation =
        By.XPath("//div[9]//md-option[32]");

    private readonly 
        By ChooseCommitedUsage =
        By.XPath("//body//div[19]/div[1]//md-select-value/span[2]");

    private readonly 
        By CommitedUsage1Year =
        By.XPath("//div[10]/md-select-menu/md-content/md-option[2]");

    private readonly 
        By AddToEstimateButton =
        By.XPath("//md-card-content[1]//md-card//form/div[20]/button");

    private readonly 
        By EstimatedCost =
        By.XPath("//md-content/md-card/div//md-content/md-list//sub/b");

    private readonly 
        By EmailEstimateButton =
        By.XPath("//md-card-content[2]//div[3]/button[2]");

    private readonly 
        By EmailYourEstimateForm =
        By.XPath("/html/body//md-dialog");

    private readonly 
        By EmailFieldAtForm = 
        By.XPath("//div[8]/md-dialog/form//div[3]/md-input-container/input");

    private readonly 
        By SendEmailButton =
        By.XPath("//body//md-dialog/form/md-dialog-actions/button[2]");

    public CalculatorPage(IWebDriver driver)
    {
        this.driver = driver;
    }

    public void ClickOnComputeEngine()
    {
        var waiter = new WebDriverWait(driver, TimeSpan.FromSeconds(10));
        var element = waiter.Until(w => w.FindElement(IFrame1BeforeComputeEngine));
        driver.SwitchTo().Frame(element);
        Thread.Sleep(3000);
        element = driver.FindElement(IFrame2BeforeComputeEngine);
        driver.SwitchTo().Frame(element);
        element = driver.FindElement(ComputeEngine);
        element.Click();
    }

    public void InputNumberOfInstances()
    {
        var waiter = new WebDriverWait(driver, TimeSpan.FromSeconds(10));
        var element = waiter.Until(w => w.FindElement(NumberOfInstances));
        element.Click();
        element.SendKeys("4");
    }

    public void SelectSeries()
    {
        var waiter = new WebDriverWait(driver, TimeSpan.FromSeconds(10));
        var element = waiter.Until(w => w.FindElement(ChooseSeries));
        element.Click();
        Thread.Sleep(2000);
        element = waiter.Until(w => w.FindElement(SeriesN1));
        element.Click();
    }

    public void SelectMachineType()
    {
        var waiter = new WebDriverWait(driver, TimeSpan.FromSeconds(10));
        var element = waiter.Until(w => w.FindElement(ChooseMachineType));
        element.Click();
        Thread.Sleep(2000);
        element = waiter.Until(w => w.FindElement(MachineType));
        element.Click();
    }

    public void CheckBoxAddGPUs()
    {
        var waiter = new WebDriverWait(driver, TimeSpan.FromSeconds(10));
        var element = waiter.Until(w => w.FindElement(AddGPUs));
        element.Click();
        Thread.Sleep(2000);
    }

    public void SelectGPUType()
    {
        var waiter = new WebDriverWait(driver, TimeSpan.FromSeconds(10));
        var element = waiter.Until(w => w.FindElement(ChooseGPUType));
        element.Click();
        Thread.Sleep(3000);
        element = waiter.Until(w => w.FindElement(GPUTypeTeslaV100));
        element.Click();
        Thread.Sleep(3000);
    }

    public void SeleсtNumberOfGPUs()
    {
        var waiter = new WebDriverWait(driver, TimeSpan.FromSeconds(10));
        var element = waiter.Until(w => w.FindElement(ChooseNumberOfGPUs));
        element.Click();
        Thread.Sleep(2000);
        element = waiter.Until(w => w.FindElement(NumberOfGPUs1));
        element.Click();
        Thread.Sleep(3000);
    }

    public void SelectLocalSSD()
    {
        var waiter = new WebDriverWait(driver, TimeSpan.FromSeconds(10));
        var element = waiter.Until(w => w.FindElement(ChooseLocalSSD));
        element.Click();
        Thread.Sleep(2000);
        element = waiter.Until(w => w.FindElement(LocalSSD2x375GB));
        element.Click();
    }

    public void SelectDatacenterLocation()
    {
        var waiter = new WebDriverWait(driver, TimeSpan.FromSeconds(10));
        var element = waiter.Until(w => w.FindElement(ChooseDatacenterLocation));
        element.Click();
        Thread.Sleep(2000);
        element = waiter.Until(w => w.FindElement(DatacenterLocation));
        element.Click();
    }

    public void SelectCommitedUsage()
    {
        var waiter = new WebDriverWait(driver, TimeSpan.FromSeconds(10));
        var element = waiter.Until(w => w.FindElement(ChooseCommitedUsage));
        element.Click();
        Thread.Sleep(2000);
        element = waiter.Until(w => w.FindElement(CommitedUsage1Year));
        element.Click();
        Thread.Sleep(3000);
    }

    public void ClickAddToEstimate()
    {
        var waiter = new WebDriverWait(driver, TimeSpan.FromSeconds(10));
        var element = waiter.Until(w => w.FindElement(AddToEstimateButton));
        element.Click();
        Thread.Sleep(3000);
    }

    public string GetTotalEstimatedMonthlyCost()
    {
        var waiter = new WebDriverWait(driver, TimeSpan.FromSeconds(10));
        var element = waiter.Until(w => w.FindElement(EstimatedCost));
        string cost = element.Text;
        return cost;
    }

    public void OpenEmailEstimateForm()
    {
        var waiter = new WebDriverWait(driver, TimeSpan.FromSeconds(10));
        var element = waiter.Until(w => w.FindElement(EmailEstimateButton));
        element.Click();
    }

    public void SendEstimateForm(string email)
    {
        var waiter = new WebDriverWait(driver, TimeSpan.FromSeconds(10));
        var element = waiter.Until(w => w.FindElement(IFrame1BeforeComputeEngine));
        driver.SwitchTo().Frame(element);
        element = driver.FindElement(IFrame2BeforeComputeEngine);
        driver.SwitchTo().Frame(element);
        element = waiter.Until(w => w.FindElement(EmailYourEstimateForm));
        element = waiter.Until(w => w.FindElement(EmailFieldAtForm));
        element.SendKeys(email);
        element = waiter.Until(w => w.FindElement(SendEmailButton));
        element.Click();
    }
}
